import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';
import MatrixRowTitle from './MatrixRowTitle';

export class TrueFalseMatrixOption extends BaseOption {
    // 选择的值
       public SelValue: boolean = false;
        // 判断结果
       public TureOrFalse: boolean = false;

       public BlankValues: string[] = [];
}

export default class TrueFalseMatrixSelSubject extends BaseQuestionSubject<TrueFalseMatrixOption> {
    public static option: TrueFalseMatrixOption;

    // 行标题
    public RowTitles: MatrixRowTitle[] = [];
    public RowTitleText: String = '';
    // 判断条件
    public Condition: string = '判断条件说明';
    // 判断标题
    public TrueTitle: string = '是';
    public FalseTitle: string = '否';
     // 选项大标题
    public OptionsTitle: string = '选项大标题';



}
